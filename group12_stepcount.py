# Team members (e-mail, legi):
# adrian.esser@hest.ethz.ch, 17-937-954
# bhallworth@ethz.ch, 21-943-154
# postepha@ethz.ch, 17-947-557

import sys
from Lilygo.Recording import Recording, data_integrity
from Lilygo.Dataset import Dataset

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal
import pycwt
import math
from os import listdir
import glob
import csv
#For interactive graphs
#matplotlib widget

#import time

###############################################################################
#                        Function Definitions                                 #
###############################################################################


def gridSearch():

    N_scoring = [5, 10, 15, 20, 25, 30]  # N_scoring
    N_peaks = [200, 300, 400, 500]  # N_peaks
    c_peaks = [-0.2, -0.1, 0, 0.1, 0.2]  # c_peaks

    groundT = [83, 50, 50, 50, 50, 50, 50, 115, 120, 100, 80, 170, 71, 50, 90, 200, 440, 160, 307, 371, 41, 101, 44, 47, 45, 33]

    with open('wpd_finetune.csv', 'w+') as f:
        fields = ['N_scoring', 'N_peaks', 'c_peaks']
        write = csv.writer(f)
        write.writerow(fields)

        for n_scoring in N_scoring:
            for n_peaks in N_peaks:
                for c in c_peaks:
                    # For testing only
                    files = glob.glob('data/Good/*.json')
                    files = sorted(files)

                    fileN = 0
                    tempRes = np.zeros(len(files))
                    while fileN < len(files):
                        try:
                            tr = Recording(files[fileN], no_labels=True, mute=True)
                        except:
                            fileN = fileN + 1
                            tr = Recording(files[fileN], no_labels=True, mute=True)
                        acc_x = tr.data['ax']
                        acc_y = tr.data['ay']
                        acc_z = tr.data['az']

                        steps = windowed_peak_detection(acc_x, acc_y, acc_z, n_scoring, n_peaks, c)
                        print("N_scoring = " + str(n_scoring) + ", N_peaks = " + str(n_peaks) + ", c_peaks = " + str(c) + ", Steps = " + str(steps))
                        tempRes[fileN] = steps - groundT[fileN]
                        fileN = fileN+1
                    newRes = [n_scoring, n_peaks, c]
                    newRes = np.concatenate((newRes, tempRes), axis=0)
                    write.writerow(newRes)
    return


def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w


def lowpass(data: np.ndarray, cutoff: float, fs: float) -> np.ndarray:
    # Gaussian lowpass filter
    sigma = fs / (2 * np.pi * cutoff)
    gx = np.linspace(-3*sigma, 3*sigma, int(6*sigma-1))
    gaussian = np.exp(-0.5*(gx/sigma)**2) / (np.sqrt(2*np.pi) * sigma)
    offset = int(((6*sigma-1)-1)/2)
    conv = np.convolve(data, gaussian, mode='valid')
    data_LP = conv[-1]*np.ones(len(data))
    data_LP[0:offset] = conv[0]*np.ones(offset)
    data_LP[offset:offset + len(conv)] = conv
    return data_LP


def scoring_peaks(data: np.ndarray, n: int) -> np.ndarray:
    # This function accentuates the peaks
    # Averages the maximum difference between the n samples to the right & left of the current sample
    # 0.5 * (max(x_i - x_i-k) + max(x_i - x_i+k)) where k = 1,...,n
    score = np.zeros_like(data)

    for i in range(len(data)):
        if i < n:
            left = data[:i]
        else:
            left = data[i-n:i]
        right = data[i+1:i+1+n]
        max_left = max(data[i] - left, default=0)
        max_right = max(data[i] - right, default=0)
        s_i = (max_right + max_left) / 2
        score[i] = s_i

    return score


def extract_peaks(data: np.ndarray, n: int, c: float) -> np.ndarray:
    # This function extracts all possible peaks
    # Guides of a window of size 2*n over the samples and only keeps points higher than the average within the window
    keep = np.copy(data)
    for i in range(len(data)):
        if i < n:
            window = data[:i+1+n]
        else:
            window = data[i-n:i+1+n]
        mu = np.mean(window)
        std = np.std(window)
        if (data[i] - mu) <= c*std:
            keep[i] = 0
    return keep


def windowed_peak_detection(a_x: Dataset, a_y: Dataset, a_z: Dataset) -> int:
    # Optimization parameters
    cutoff = 5  # 5 Hz cutoff frequency
    N_scoring = 60  # Window size for scoring
    f_max = 3.5  # Maximum running frequency. determines minimum distance between peaks
    N_peaks = 300  # Window size for finding peaks
    c_peaks = 0.35  # Factor determining what defines a peak (value - mean >! c * std)

    a = np.power(a_x.values, 2) + np.power(a_y.values, 2) + np.power(a_z.values, 2)
    t = np.arange(0, len(a))

    # Filter out High frequencies that cannot correspond to walking/running
    fs = a_x.samplerate  # Sample rate
    a_LP = lowpass(a, cutoff, fs)

    # Score/accentuate the peaks.
    a_scored = scoring_peaks(a_LP, N_scoring)
    a_scored_LP = lowpass(a_scored, cutoff, fs)

    # Moves a window over the samples and filters out any points that cannot be peaks
    a_peaks = extract_peaks(a_scored_LP, N_peaks, c_peaks)

    peak_ind = scipy.signal.find_peaks(a_peaks, height=0.25, distance=fs/f_max, prominence=0.2)[0]

    [t_steps, steps] = temporal_binning(peak_ind, a_peaks[peak_ind], 3*fs, 4)

    # Plotting. Remove once convinced it works
    
    '''
    fig, plot = plt.subplots(nrows=4, ncols=1, sharex=True)
    plot[0].plot(t, a)
    plot[0].plot(t, a_LP)
    plot[0].set_title('Raw Data & Filtered Raw Data')
    plot[1].plot(t, a_scored)
    plot[1].plot(t, a_scored_LP)
    plot[1].set_title('Scored Data & Filtered Scored Data')
    plot[2].plot(t, a_peaks)
    plot[2].plot(peak_ind, a_peaks[peak_ind], 'rx')
    plot[2].set_title('Extracted Peaks')
    plot[3].vlines(t_steps, ymin=0, ymax=steps)
    plot[3].set_title('Extracted Steps')
    plt.show()
    '''

    return len(steps)


def cwt(ax, ay, az):

    fs = (ax.samplerate)    # Sample freq
    dt = 1 / fs

    # Parameters to tune
    freq_band = np.linspace(0.35, 3.75, num=101)  #frequency band to extract
    movAvrWin = 0.25    # window for smoothing in seconds
    peakSpacing = 0.25  # Min peak spacing in seconds
    peakHeight = 0.28     # threshold for extracting

    cutoff = np.max(freq_band)
    #sigma = fs / (2 * np.pi * cutoff)
    sigma = 6
    wvlt = pycwt.mothers.Morlet(sigma)
    a_norm = np.sqrt(np.power(ax.values, 2) + np.power(ay.values, 2) + np.power(az.values, 2))

    a_transform, sj, freqs_out, coi, fft, fftfreqs = pycwt.cwt(a_norm, dt, freqs=freq_band, wavelet=wvlt)
    a_reconst = np.real(pycwt.icwt(a_transform[:, :], sj, dt, math.log((sj[1] / sj[0]), 2), wavelet=wvlt))
    a_reconst = a_reconst[int(0.15*fs):int(-0.15*fs)]

    smoothedTrace = moving_average(a_reconst, int(movAvrWin * fs))
    steps = scipy.signal.find_peaks(smoothedTrace, height=peakHeight, distance=(fs * peakSpacing), prominence=1.2*peakHeight)[0]

    [t_steps, steps] = temporal_binning(steps, smoothedTrace[steps], 3*fs, 4)

    '''
    plt.plot(smoothedTrace)
    plt.plot(t_steps, steps, 'rx')
    #plt.plot(steps, smoothedTrace[steps], 'rx')
    plt.show()
    '''
    
    return len(steps)

'''
Takes 1-D array x (with measurements at corresponding timepoints in t)
and segments data into groups that have at temporal gaps of at least min_temporal_gap.
min_group_size is a tuning threshold that dictates how many elements a group must have to be kept.
The (potentially) reduced time and values are returned as np.arrays

For Example:

t = np.asarray([1, 2, 3, 4, 5, 10, 11, 12, 20, 25, 26, 27, 28, 29, 30, 50, 60, 80, 81, 82, 84, 86, 88, 100, 101, 102, 103, 120, 125])
x = np.linspace(1,len(t),len(t))
t_binned, x_binned = temporal_binning(t, x, 3, 5)
print(t_binned)
print(x_binned)
'''
def temporal_binning(t, x, min_temporal_gap, min_group_size):
    t = np.asarray(t)
    x = np.asarray(x)
    t_binned = []
    x_binned = []

    dt = t[1:]-t[0:-1]
    ind_gaps = np.where(dt > min_temporal_gap)[0]
    ind_gaps = np.append(ind_gaps, len(t)-1)
    
    for i in range(0,len(ind_gaps)):
        if i == 0:
            i_start = 0
            i_end = ind_gaps[i]
        elif i == (len(ind_gaps)):
            i_start = ind_gaps[i]
            i_end = len(t)-1
        else:
            i_start = ind_gaps[i-1] + 1
            i_end = ind_gaps[i]
        slice_length = i_end-i_start+1
   
        if slice_length >= min_group_size:
            t_binned.extend(t[i_start:i_end+1].tolist())
            x_binned.extend(x[i_start:i_end+1].tolist())
                    
    # Last group at the end
    i_start = ind_gaps[-1]+1
    i_end = len(t)-1

    slice_length = i_end-i_start+1
    if slice_length >= min_group_size:
        t_binned.extend(t[i_start:i_end+1].tolist())
        x_binned.extend(x[i_start:i_end+1].tolist())
    
    return np.asarray(t_binned), np.asarray(x_binned)


###############################################################################
#                               Code                                          #
###############################################################################
filename = sys.argv[1]  # e.g. 'data/someDataTrace.json' TODO: DON'T FORGET TO SWITCH ME BACK!!!

# IMPORTANT: To allow grading, the two arguments no_labels and mute must be set True in the constructor when loading the data
trace = Recording(filename, no_labels=True, mute=True)
#trace.DataIntegrityCheck()

# X-axis of the LILYGO accelerometer
ax = trace.data['ax']
ay = trace.data['ay']
az = trace.data['az']

#
# Your algorithm goes here

#stepInds = cwt(ax,ay,az)

# Make sure, you only use data from the LilyGo Wristband, namely the following 10 keys (as in trace.data[key]):
# 3-axis accelerometer: key in [ax, ay, az]
# 3-axis gyro: key in [gx, gy, gz]
# 3-axis magnetometer: key in [mx, my, mz] 
# IMU temperature: key==temperature
#
'''
files = glob.glob('data/Good/*.json')
files = sorted(files)
steps_wpd = {}
steps_cwt = {}
for file in files:
    trace = Recording(file, no_labels=True, mute=True)
    
    #start = time.time()
    step = windowed_peak_detection(trace.data['ax'], trace.data['ay'], trace.data['az'])
    #end = time.time()
    #print("WPD Time: ", end - start)

    steps_wpd[file] = step

    #start = time.time()
    step = cwt(trace.data['ax'], trace.data['ay'], trace.data['az'])
    #end = time.time()
    #print("CWT Time: ", end - start)

    steps_cwt[file] = step

save = open('output.csv', 'w')
writer = csv.writer(save)
writer.writerow(steps_wpd.keys())
writer.writerow(steps_wpd.values())
writer.writerow(steps_cwt.values())
save.close()
sys.exit()
'''

#gridsearch()
wpd = windowed_peak_detection(ax, ay, az)  # <- here goes your detected stepcount
cwt = cwt(ax, ay, az)

#print("wpd: ", wpd)
#print("cwt: ", cwt)
stepCount = int(np.ceil((wpd + cwt) / 2))
#Print result as Integer, do not change!
print(stepCount)

# Test this file before submission with the data we provide to you

# 1. In the console execute:
# python --version

# Output should look something like (displaying your python version, which must be >3):
# Python 3.9.7
# If not, check your python installation or command 

# 2. In the console execute:
# python [thisfilename.py] path/to/datafile.json

# Output should be an integer corresponding to the step count you calculated
