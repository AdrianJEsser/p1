These are traces similar to the ones used to test your step counting algorithms.
These released traces are recorded by the same person using the same hardware (watch, phone) as the ones used to test your algorithm. However, your algorithm will be tested with a larger, more diverse dataset.
We recommend you to record your own data to build a robust algorithm.

The released traces:

released_0: 41 steps (walking, arm still)
released_1: 101 steps (walking, arm swinging)
released_2: 44 steps (running)
released_3: 47 steps (walking downhill)
released_4: 45 steps (walking, tie shoelaces)
released_5: 33 steps (walking/climbing stairs)

The number of steps were counted manually during recording, using a tally counter held in the hand opposite to the wristwatch (to not disturb measurements of the watch).

Note: When loading this data, you may get error/warning messages because we removed all data traces that must not be used for classification. Make sure that in your submission both attributes to Recording() no_labels=True and mute=True are set to True to avoid these messages.