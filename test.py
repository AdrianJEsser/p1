# Team members (e-mail, legi):
# adrian.esser@hest.ethz.ch, 17-937-954
# bhallworth@ethz.ch, 21-943-154
# postepha@ethz.ch, 17-947-557
import os
import sys
from Lilygo.Recording import Recording, data_integrity
from Lilygo.Dataset import Dataset

import matplotlib.pyplot as plt
import numpy as np
import scipy.signal
import pycwt
import warnings
import math
from os import listdir
import pathlib
import glob
warnings.filterwarnings("ignore")

#For interactive graphs
#matplotlib widget

###############################################################################
#                        Function Definitions                                 #
###############################################################################
f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True, sharey=True)
#channels, axs = plt.subplots(nrows=3, ncols=2)

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w

def cwt(ax, ay, az,g=6):
    sigma = 6
    freq_band = np.linspace(0.5,3,num=300)  #frequency band to extract
    #all_band = np.linspace(0.1,100,num=1000)  # frequency band to extract
    #freq_band = freq_band/(2*np.pi)
    widths = sigma/(2*freq_band*np.pi)
    fs = int(ax.samplerate)
    a_norm = np.sqrt(np.power(ax.values, 2) + np.power(ay, 2) + np.power(az, 2))
    b, a = scipy.signal.butter(2, 7/(fs/2), btype='low')

    #a_norm = scipy.signal.lfilter(b, a, a_norm)

    dt = 1/fs



    a_transform,sj,freqs_out,coi,fft,fftfreqs = pycwt.cwt(a_norm, dt, freqs = freq_band, wavelet=pycwt.Morlet(sigma))
    freqs_keep = freqs_out[8:50]
    sj_keep = sj[2:24]
    widths_keep = widths[8:50]
    a_transform_keep = a_transform[8:50,:]
    #a_reconst = pycwt.icwt(a_transform_keep, widths_keep, dt)
    #a_transform = scipy.signal.cwt(a_norm, scipy.signal.morlet2,widths, w=sigma)
    #a_reconst = pycwt.icwt(a_transform,widths, dt)
    a_reconst = np.real((pycwt.icwt(a_transform, sj, dt, math.log((sj[1]/sj[0]),2))))

    energy_rat = np.square(a_reconst)/np.square(a_norm)


    ax1.plot(a_norm)
    ax2.plot(a_reconst)
    #ax3.plot(energy_rat)

    #plt.legend([ax1,ax2], ["IMU","CWT"])
    nSteps = 0

    nSteps = MCC(a_reconst,fs)
    #print(nSteps)
    return nSteps
   # if g<3:
   #     axs[0,g-1].plot(a_reconst)
   # elif g<5:
   #     axs[1,g-3].plot(a_reconst)
   # else:
   #     axs[2, g - 5].plot(a_reconst)
   #     justMcc = MCC(a_norm, fs)
   #     print("JustMCC ", justMcc)

def MCC(traceIn, fs):
    movAvrWin = 0.29 #window in seconds
    meanWin = 2

    winSize = int(meanWin*fs)

    smoothedTrace = moving_average(traceIn, int(movAvrWin*fs))
    nSteps = 0
    for i in range(0,int(len(smoothedTrace)/winSize)):
        window = smoothedTrace[(i*winSize):((i+1)*winSize)]
        winMean = np.mean(window)
        window = window-winMean
        winSteps = (((window[:-1] * window[1:]) < 0) & ((window[:-1] < window[1:]))).sum()  #count positive zero-crossings
        nSteps = nSteps+winSteps
        #ax3.plot(list(range((i * winSize),(i + 1) * winSize)),window)
    if ((i+1)*winSize) < len(smoothedTrace):
        window = smoothedTrace[((i + 1) * winSize):]
        winMean = np.mean(window)
        window = window - winMean
        winSteps = (((window[:-1] * window[1:]) < 0) & ((window[:-1] < window[1:]))).sum() # count positive zero-crossings
        nSteps = nSteps + winSteps
    return nSteps
###############################################################################
#                               Code                                          #
###############################################################################
#filename = sys.argv[1] # e.g. 'data/someDataTrace.json'

print(listdir('data'))
fileDir = 'data'
#files = listdir('data')
#files = list(pathlib.Path('data/').glob('*.json'))
files = glob.glob('data/*.json')
#filename = 'data/lilygo_15_03_15_04_31.json'
#print(filename)
results = np.zeros([len(files),2])


for fileInd in range(len(files)):
    filename = files[fileInd]
    trace = Recording(filename, no_labels=True, mute=True)
    trace.DataIntegrityCheck()
    ax = trace.data['ax']
    ay = trace.data['ay']
    az = trace.data['az']
    gx = trace.data['gx']
    gy = trace.data['gy']
    gz = trace.data['gz']
    mx = trace.data['mx']
    my = trace.data['my']
    mz = trace.data['mz']
    results[fileInd, 0] = fileInd+1
    results[fileInd,1]=cwt(ax, ay.values, az.values, 6)

# IMPORTANT: To allow grading, the two arguments no_labels and mute must be set True in the constructor when loading the data
trace = Recording(filename, no_labels=True, mute=True)
trace.DataIntegrityCheck()

#The data is stored inside a dictionary (access: trace.data["key"])
print("\r\nAvailable data traces:")
print(list(trace.data.keys()))

# X-axis of the LILYGO accelerometer
ax = trace.data['ax']
ay = trace.data['ay']
az = trace.data['az']
gx = trace.data['gx']
gy = trace.data['gy']
gz = trace.data['gz']
mx = trace.data['mx']
my = trace.data['my']
mz = trace.data['mz']

print(f"Name of sensor: '{ax.title}'")
print(f"Sample rate: {int(ax.samplerate)} Hz")
print(f"Recording length: {ax.total_time} seconds")
print(f"Timestamp of recording: {ax.raw_timestamps[0][1]}")
print("\nGetting a time series:")
print(" Time           x-Accel")
for x, t in list(zip(ax.timestamps, ax.values))[:20]:
    print(f"{x: .3f}s \t{t:.5f}g")

# Display a simple plot
#trace.plot(['ax'])

# Plot multiple axis at the same time
# It's also possible to give titles to the y-axis and plots
#trace.plot(['ax', 'ay', 'az'], ylabels=['Accelerometer [g]'], labels=['Acc X', 'Acc Y', 'Acc Z'])

# Plot multiple sensors and multiple axis at the same time
'''
trace.plot([['ax', 'ay', 'az'], ['gx', 'gy', 'gz'], ['mx', 'my', 'mz'], ['altitude'], ['speed']],
             ylabels=['Accelerometer [g]', 'Gyroscope', 'Magnetometer', 'Elevation (m)', 'speed'],
             labels=[['Acc X', 'Acc Y', 'Acc Z'], ['Gyro X', 'Gyro Y', 'Gyro Z'], ['Mag X', 'Mag Y', 'Mag Z'], ['altitude'], ['speed']])
'''
#
# Your algorithm goes here
# Make sure, you only use data from the LilyGo Wristband, namely the following 10 keys (as in trace.data[key]):
# 3-axis accelerometer: key in [ax, ay, az]
# 3-axis gyro: key in [gx, gy, gz]
# 3-axis magnetometer: key in [mx, my, mz]
# IMU temperature: key==temperature
#
a_norm = np.power(ax.values, 2) + np.power(ay.values, 2) + np.power(az.values, 2)

#TODO: Moving average filter should be done as a function of temporal window length
#      based off of sampling rate (for robustness).
a_norm_filt = moving_average(a_norm, 40)

peak_inds = scipy.signal.find_peaks(a_norm_filt, height=1.0, threshold=None, distance=20)[0]
print(peak_inds)

#plt.plot(a_norm)
#plt.plot(a_norm_filt)
#plt.plot(peak_inds, a_norm_filt[peak_inds], 'r*')
#cwt(ax,ay,az)
#print("Acc X")
#cwt(ax,np.zeros(len(ax.values)),np.zeros(len(ax.values)),1)
#print("Acc Y")
#cwt(ay,np.zeros(len(ax.values)),np.zeros(len(ax.values)),2)
#print("Acc X")
#cwt(az,np.zeros(len(az.values)),np.zeros(len(az.values)),3)
#print("Gyr X")
#cwt(gx,np.zeros(len(gx.values)),np.zeros(len(gx.values)),4)
#print("Gyr Y")
#cwt(gy,np.zeros(len(gy.values)),np.zeros(len(gx.values)),5)
#print("GYR Z")
#cwt(gz,np.zeros(len(gy.values)),np.zeros(len(gx.values)),6)
print("GYR All")
#cwt(gx,gy.values,gz.values,6)
print("Acc All")
cwt(ax,ay.values,az.values,6)
#channels.savefig('6chan.png', dpi=300)
plt.grid()
plt.show()

stepCount = len(peak_inds) # <- here goes your detected stepcount
#Print result as Integer, do not change!
print(stepCount)

# Test this file before submission with the data we provide to you

# 1. In the console execute:
# python --version

# Output should look something like (displaying your python version, which must be >3):
# Python 3.9.7
# If not, check your python installation or command

# 2. In the console execute:
# python [thisfilename.py] path/to/datafile.json

# Output should be an integer corresponding to the step count you calculated

